# Ansible Role: scc\_net.web.dehydrated\_domains

Generate domains.d file and assemble dehydrated domains

## Dependencies

- scc_net.web.dehydrated

## Role Variables

```yaml
dehydrated_domains_list: [['service1.example.com', 'alias1.example.com'], ['service2.example.com']]
```
