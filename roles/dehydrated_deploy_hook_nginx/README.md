# Ansible Role: dehydrated_deploy_hook_nginx

This role configures a dehydrated deploy hook for nginx

## Dependencies

- scc_net.web.dehydrated
