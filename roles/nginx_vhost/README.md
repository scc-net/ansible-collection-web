# Ansible Role: scc\_net.web.nginx\_vhost

Manage nginx vhost config files.

## Dependencies

- scc_net.firewall.rule
- scc_net.web.dehydrated_deploy_hook_nginx
- scc_net.web.dehydrated_domains
- scc_net.web.nginx

## Role Variables

```
nginx_vhost: {}
```

## Example config

Example vhost below, showing all available options:

```
nginx_vhost_ips_access_allowed:
  - "2001:db8:1::/64"
  - "2001:db8:1000::/64"
nginx_vhost:
  listen:
    - "443" # default: "443"
  server_names:
    - "example.com" # default: N/A
  root: "/var/www/example.com" # default: N/A
  index: "index.html index.htm" # default: "index.html index.htm"
  filename: "example.com.conf" # Can be used to set the vhost filename.
  # Properties that are only added if defined:
  server_names_redirect:
    - "www.example.com" # default: N/A
  error_page: ""
  access_log: ""
  error_log: ""
  extra_parameters: "" # Can be used to add extra config blocks (multiline).
  template: "" # Can be used to override the `nginx_vhost_template` per host.
  state: "absent" # To remove the vhost configuration.
```
