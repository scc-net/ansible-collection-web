## php_fpm_pool

Pool socket path is provided through the variable `php_fpm_pool_socket_path`.

Available pool variables:

```yaml
php_fpm_pool:
  name: app.example.org
  user: www-app
  group: www-data
  listen_owner: www-data
  listen_group: www-data
  listen_mode: 0660
  pm: dynamic
  pm_max_children: 5
  pm_start_servers: 2
  pm_process_idle_timeout: 10s
  pm_min_spare_servers: 1
  pm_max_spare_servers: 3
  pm_max_requests: 500
  php_env:
    HOSTNAME: $HOSTNAME
    TMP: /tmp
    TMPDIR: /tmp
  php_value:
    error_log: /var/log/app/error.log
  php_flag:
    log_errors: True
  php_admin_value:
    sendmail_path: /usr/sbin/sendmail
  php_admin_flag:
    display_errors: False
```

## Dependencies

- scc_net.web.php_fpm
