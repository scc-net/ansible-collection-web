# uwsgi role

Role for deployment of uwsgi applications with systemd ans socket activation

## usage

This role installs all for uwsgi apps required packages and deploys uwsgi apps with their own user if provided.

example variables for this role
```yaml
uwsgi_app:
  name: demoapp
  python:
    project_folder: "demoapp"
    venv_folder: "venv"
    module: demoapp.wsgi
    mount:
      path: "/"
      package: "demoapp"
      app_instance: "app"
  env:
    ENV_VAR: ENV_CONTENT
```

Following variables are available after role execution:
```yaml
uwsgi_venv_path: path of the venv
uwsgi_user: result of ansible user module for uwsgi user
```
