## php_fpm

Installs and configures basic settings for php-fpm on Debian systems.

Availables Settings (with defaults):
```yaml
# PHP-FPM pool configurations
php_fpm_remove_default_www: True
php_fpm_pool: {}

# PHP debian default configuration parameters
php_fpm_ini_short_open_tag: "Off"
php_fpm_ini_max_execution_time: 30
php_fpm_ini_max_input_time: 60
php_fpm_ini_memory_limit: 128M
php_fpm_ini_error_reporting: E_ALL & ~E_DEPRECATED & ~E_STRICT
php_fpm_ini_display_errors: "Off"
php_fpm_ini_post_max_size: 8M
php_fpm_ini_upload_max_filesize: 2M

php_fpm_pid_file: "/run/php/php{{ php_version }}-fpm.pid"
php_fpm_log_level: notice
```