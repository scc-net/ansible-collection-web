#!/usr/bin/env python3
# -*- coding: utf-8 -*

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import base64
from ansible.module_utils._text import to_text, to_bytes


def b64urlencode(string, encoding="utf-8"):
    return to_text(base64.urlsafe_b64encode(to_bytes(string, encoding=encoding, errors='surrogate_or_strict'))).rstrip("=")


class FilterModule(object):
    """ Ansible custom jinja2 filters """

    def filters(self):
        return {
            # base64 urlencode
            "b64urlencode": b64urlencode
        }

