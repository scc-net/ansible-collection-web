# Ansible Collection - scc_net.web

Documentation for the colleciton.

## Included plugins

### scc_net.web.b64urlencode

## Included roles

### scc_net.web.dehydrated

Configures and installs dehydrated

[Documentation](roles/dehydrated/README.md)

### scc_net.web.dehydrated_deploy_hook_apache

Deploy Hook helper role for apache services

### scc_net.web.dehydrated_deploy_hook_nginx

Deploy Hook helper role for nginx services

### scc_net.web.dehydrated_domains

Deploys domains.d file for dehydrated

### scc_net.web.nginx

Configures and installs nginx

[Documentation](roles/nginx/README.md)

### scc_net.web.php_fpm

[Documentation](roles/php_fpm/README.md)

### scc_net.web.php_fpm_pool

[Documentation](roles/php_fpm_pool/README.md)

### scc_net.web.uwsgi

[Documentation](roles/uwsgi/README.md)

### scc_net.web.nginx_vhost

Deploys vhost config files

### scc_net.web.haproxy

HAProxy base role
